package Elements;

import com.mxgraph.model.mxGeometry;

/**
 * Процесс (задание)
 */
public class Task extends BasicElement{
    public Task()
    {
        super();
        // Установка подписи по умолчанию
        this.setValue("Задание");
        // Установка начальной геометрии
        this.setGeometry(new mxGeometry(50,10,200,50));
        // Установка стилей элемента
        addStyle("rounded","1");
        addStyle("fillColor","white");
        addStyle("strokeWidth","2");
        addStyle("strokeColor","black");
        addStyle("whiteSpace","wrap");//распределение текста по всему элементу
        addStyle("spacing","10");
        addStyle("verticalAlign","top");
        // Установка максимальной длины отображаемого текста
        visibleTextLenght = 20;
    }
}
